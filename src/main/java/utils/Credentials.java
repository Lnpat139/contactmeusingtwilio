package utils;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Credentials {

    private static Logger logger = LoggerFactory.getLogger(Client.class);


    public static final String USERS_TWILIO_CREDS_TXT = "/Users/lpatil/Downloads/Twilio/creds.txt";
    private Map<String, String> env;

    public Credentials() throws FileNotFoundException {
        try {
            env = initializeEnv();
        } catch (FileNotFoundException e) {
            logger.info(String.format("File %s not found in the system!", USERS_TWILIO_CREDS_TXT), e);
            env = System.getenv();
        }
     }

    public String getAccountSid() {
        return env.get("TWILIO_ACCOUNT_SID");
    }

    public String getAuthToken() {
        return env.get("TWILIO_AUTH_TOKEN");
    }

    public String getPhoneNumber() {
        return env.get("TWILIO_PHONE_NUMBER");
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Map<String, String> initializeEnv() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(USERS_TWILIO_CREDS_TXT));
        Map<String, String> envMap = new LinkedHashMap<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] propertyValueArray = line.split("=");
            envMap.put(propertyValueArray[0], propertyValueArray[1]);
        }
        return envMap;
    }
}
